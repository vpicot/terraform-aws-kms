output "account_id" {
  value = data.aws_caller_identity.current.account_id
}

output "kms_key_arn" {
  value       = aws_kms_key.kms_key.arn
  description = "The Amazon Resource Name (ARN) of the key"
}

output "kms_key_id" {
  value       = aws_kms_key.kms_key.key_id
  description = "The globally unique identifier for the key"
}

output "kms_key_alias_arn" {
  value       = aws_kms_alias.kms_key_alias.arn
  description = "The Amazon Resource Name(ARN) of the key alias"
}

output "kms_key_alias_id" {
  value = aws_kms_alias.kms_key_alias.id
  description = "The Amazon Resource Name(ARN) of the key alias"
}
