 // ----- Mandatory tags ----- 

variable "security" {
  type        = string
  description = "The urbassec zone of the account"
  validation {
    condition     = var.security == lower(var.security)
    error_message = "The subnet zone must be in lowercase."
  }
  validation {
    condition     = can(regex("^z[0-2]?[0-9]$", var.security))
    error_message = "The subnet zone must be in format: zi (^z[0-2]?[0-9]$)."
  }
}

variable "environment" {
  type        = string
  description = "The environment name: plg/dev/npd/ppd/prd."
  validation {
    condition     = contains(["plg", "dev", "npd", "ppd", "prd"], var.environment)
    error_message = "The environment must be a valide value or empty: plg/dev/npd/ppd/prd."
  }
}

variable "company" {
  type        = string
  description = "The trigram of the company."
  validation {
    condition     = length(var.company) == 3
    error_message = "The company is mandatory and must be a trigram."
  }
  validation {
    condition     = var.company == lower(var.company)
    error_message = "The company must be in lowercase."
  }
}

variable "service_id" {
  type        = string
  description = "The service portfolio or serviceID."
  validation {
    condition     = length(var.service_id) <= 50
    error_message = "The serviceID must be a less than 50 characters."
  }
  validation {
    condition     = var.service_id == lower(var.service_id)
    error_message = "The serviceID must be in lowercase."
  }
}

variable "description" {
    description = "The description of the key as viewed in AWS console"
    type        = string
}

variable "key_usage" {
    description = "Specifies the intended use of the key. Valid values: ENCRYPT_DECRYPT or SIGN_VERIFY"
    type        = string
}


variable "cmk_spec" {
    description = "Specifies whether the key contains a symmetric key or an asymmetric key pair and the encryption algorithms or signing algorithms that the key supports. Valid values: SYMMETRIC_DEFAULT, RSA_2048, RSA_3072, RSA_4096, HMAC_256, ECC_NIST_P256, ECC_NIST_P384, ECC_NIST_P521, or ECC_SECG_P256K1"
    type        = string
}

variable "multi_region" {
    description = "Indicates whether the KMS key is a multi-Region (true) or regional (false) key"
    type        = string
}

variable "deletion_window_in_days" {
    description = "The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between 7 and 30"
    type        = number
}
variable "alias_name" {
    description = "Creates an unique alias beginning with the specified prefix"
    type        = string
}
variable "tags" {
    description = "A map of tags to assign to the KMS key"
    type = map
}