# Module Terraform KMS

## Main goals

With AWS Key Management Service (AWS KMS) you can create and manage cryptographic keys and control their use across a wide range of AWS services and in your applications. AWS KMS is a secure and resilient service that uses hardware security modules that have been validated under FIPS 140-2, or are in the process of being validated, to protect your keys. AWS KMS is integrated with AWS CloudTrail to provide you with logs of all key usage to help meet your regulatory and compliance needs.

With this Terraform module you can create and configure your AWS costumer managed keys

See also:

* [Terraform Documentation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key)
* [AWS Documentation](https://docs.aws.amazon.com/kms/?id=docs_gateway)

Sample usage :
```
module "kms_key" {
  source                  = "git::https://git.cloud.safran/safrangrp/publiccloud/landingzone/shared-modules/terraform-aws-kms"
  security                = var.security
  environment             = var.environment
  company                 = var.company
  service_id              = var.service_id
  description             = var.description
  key_usage               = var.key_usage
  cmk_spec                = var.cmk_spec
  is_multi_region         = var.is_multi_region
  deletion_window_in_days = var.deletion_window_in_days
  tags                    = var.tags
  alias_name              = var.alias_name
}

```

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.0.0 |
| aws | >= 4.27.0 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 4.27.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_kms_alias.kms_key_alias](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_key.kms_key](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.kms_key_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| alias\_name | Creates an unique alias beginning with the specified prefix | `string` | n/a | yes |
| cmk\_spec | Specifies whether the key contains a symmetric key or an asymmetric key pair and the encryption algorithms or signing algorithms that the key supports. Valid values: SYMMETRIC\_DEFAULT, RSA\_2048, RSA\_3072, RSA\_4096, HMAC\_256, ECC\_NIST\_P256, ECC\_NIST\_P384, ECC\_NIST\_P521, or ECC\_SECG\_P256K1 | `string` | n/a | yes |
| company | The trigram of the company | `string` | n/a | yes |
| deletion\_window\_in\_days | The waiting period, specified in number of days. After the waiting period ends, AWS KMS deletes the KMS key. If you specify a value, it must be between 7 and 30 | `number` | n/a | yes |
| description | The description of the key as viewed in AWS console | `string` | n/a | yes |
| environment | The environment name: plg/dev/npd/ppd/prd | `string` | n/a | yes |
| is\_multi\_region | Indicates whether the KMS key is a multi-Region (true) or regional (false) key | `string` | n/a | yes |
| key\_usage | Specifies the intended use of the key. Valid values: ENCRYPT\_DECRYPT or SIGN\_VERIFY | `string` | n/a | yes |
| security | The urbassec zone of the account | `string` | n/a | yes |
| service\_id | The service portfolio or serviceID | `string` | n/a | yes |
| tags | A map of tags to assign to the KMS key | `map` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| account\_id | n/a |
| kms\_key\_alias\_arn | The Amazon Resource Name(ARN) of the key alias |
| kms\_key\_alias\_id | The Amazon Resource Name(ARN) of the key alias |
| kms\_key\_arn | The Amazon Resource Name (ARN) of the key |
| kms\_key\_id | The globally unique identifier for the key |
