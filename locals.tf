locals {
  required_tags = {
    Security    = upper(var.security)
    Environment = upper(var.environment)
    Company     = upper(var.company)
    ServiceID   = upper(var.service_id)
  }
}
