resource "aws_kms_key" "kms_key" {
  description              = var.description 
  key_usage                = var.key_usage 
  customer_master_key_spec = var.cmk_spec
  enable_key_rotation      = true
  multi_region             = var.multi_region
  deletion_window_in_days  = var.deletion_window_in_days 
  policy                   = data.aws_iam_policy_document.kms_key_policy.json
  tags                     = merge(local.required_tags, var.tags)
}

resource "aws_kms_alias" "kms_key_alias" {
  name                    = "alias/${var.alias_name}"
  target_key_id           = aws_kms_key.kms_key.key_id
}
